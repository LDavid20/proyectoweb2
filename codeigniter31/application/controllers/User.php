<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function inicio()
	{
		// params
		// load data
		$this->load->view('include/head');
		$this->load->view('user/inicio');
		$this->load->view('include/footer');
	}

	// public function admin()
	// {
	// // params
	// // load data
	// 	$this->load->view('include/head');
	// 	$this->load->view('user/inicio');
	// 	$this->load->view('include/footer');
	// }

	// public function inicio()
	// {
	// 	$this->load->view('user/inicio');
	// }



	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->set_flashdata('error', 'Inicie sesión nuevamente');
		redirect(site_url(['user', 'inicio']));
	}
	public function register()
	{
		$this->load->view('include/head');
		$this->load->view('user/register');
		$this->load->view('include/footer');
	}

	public function dashboard()
	{
		if ($this->session->has_userdata('user')) {
			$authFriends = $this->user_model->getFriends();
			$this->session->set_userdata('friends', $authFriends);
			$authTrees = $this->user_model->getTrees();
			$this->session->set_userdata('trees', $authTrees);
			$this->load->view('include/head');
			$this->load->view('tree/admin');
			$this->load->view('include/footer');
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user', 'inicio']));
		}
	}

	public function adminfriends()
	{
		if ($this->session->has_userdata('user')) {
			$authFriends = $this->user_model->getFriends();
			$this->session->set_userdata('friends', $authFriends);
			$this->load->view('include/head.php');
			$this->load->view('tree/adminfriends');
			$this->load->view('include/footer');
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user', 'inicio']));
		}
	}

	public function alltree()
	{
		if ($this->session->has_userdata('user')) {
			$authTfull = $this->user_model->getTreeFriendsAll();
			$this->session->set_userdata('tfull', $authTfull);
			$this->load->view('include/head.php');
			$this->load->view('tree/alltree');
			$this->load->view('include/footer');
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user', 'inicio']));
		}
	}

	public function mytree()
	{
		if ($this->session->has_userdata('user')) {
			$authTName = $this->user_model->getTreeName();
			$this->session->set_userdata('treename', $authTName);
			$authTreeFriends = $this->user_model->getTreeFriends($this->session->user->id);
			$this->session->set_userdata('treefriend', $authTreeFriends);

			$this->load->view('include/head.php');
			$this->load->view('tree/mytree');
			$this->load->view('include/footer');
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user', 'inicio']));
		}
	}

	public function createtree()
	{
		if ($this->session->has_userdata('user')) {
			$validation_errors = [];

			if (!$this->input->post('name')) {
				$validation_errors['name'] = 'is blank';
			}
			if (!$this->input->post('donate')) {
				$validation_errors['donate'] = 'is blank';
			}
			if (!$this->input->post('idtree')) {
				$validation_errors['idtree'] = 'is blank';
			}
			if (sizeof($validation_errors) > 0) {
				$this->session->set_flashdata('error', 'Faltan ingresar datos');
				redirect(site_url(['user', 'mytree']));
			} else {
				$treeinfo = array(
					"idtree" => $this->input->post('idtree'),
					"name" => $this->input->post('name'),
					"heigth" => 1,
					"date" => date('Y-m-d'),
					"donate" => $this->input->post('donate'),
				);
				$idtreeinfo = $this->user_model->insertarTreeInfo($treeinfo);
				$tree = array(
					"idtreeinfo	" => $idtreeinfo,
					"idfriend" => $this->session->user->id
				);
				$insertTreeFriend = $this->user_model->insertarTreeFriend($tree);

				if ($insertTreeFriend) {
					$this->session->set_flashdata('error', 'Arbol Registrado con exito');
				} else {
					$this->session->set_flashdata('error', 'El arbol ya se encuentra registrado');
				}
				redirect(site_url(['user', 'mytree']));
			}
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user', 'inicio']));
		}
	}

	public function updatetree()
	{
		$validation_errors = [];

		if (!$this->input->post('name')) {
			$validation_errors['name'] = 'is blank';
		}
		if (!$this->input->post('heigth')) {
			$validation_errors['heigth'] = 'is blank';
		}
		if (!$this->input->post('idtree')) {
			$validation_errors['idtree'] = 'is blank';
		}
		if (sizeof($validation_errors) > 0) {
			$this->session->set_flashdata('error', 'Faltan ingresar datos');
			redirect(site_url(['user', 'mytree']));
		} else {
			$tree = array(
				"name" => $this->input->post('name'),
				"heigth" => $this->input->post('heigth'),
				"idtree" => $this->input->post('idtree')
			);
			$updateTree = $this->user_model->updateTree($this->session->idarbol, $tree);
			if ($updateTree) {
				$this->session->set_flashdata('error', 'Información actualizada con exito');
			} else {
				$this->session->set_flashdata('error', 'Error al actualizar la información');
			}
			redirect(site_url(['user', 'mytree']));
		}
		// redirect(site_url(['user','mytree']));	
	}
	public function namePicture($inputName)
	{
		$aa = explode('.', $inputName);
		echo $aa[0];
		echo $inputName;

		$fileObject = $_FILES[$inputName];

		// $target_dir = "images/imgtree/";
		$target_dir = "uploads/";
		$target_file = $target_dir . basename($fileObject["name"]);

		if (move_uploaded_file($fileObject["tmp_name"], $target_file)) {
			return $target_file;
		} else {
			return "";
		}
	}
	public function guardarImagen()
	{
		if ($this->session->has_userdata('user')) {
			// $mi_archivo = 'upload';
			$config['upload_path'] = "./uploads/";
			$config['file_name'] = "nombre_archivo";
			$config['allowed_types'] = "gif|jpg|png|jpeg";
			$config['max_size'] = 2000;
			$config['max_width'] = 1500;
			$config['max_height'] = 1500;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('picture')) {

				// $aa = explode('.',$this->input->GET('picture'));
				// echo $aa[0];
				// echo $inputName;
				// $a = document.getElementById("picture").value;
				//  echo $this->input->GET('picture');
				// $name = $this->namePicture($this->input->GET('picture'));

				$data['uploadError'] = $this->upload->display_errors();
				echo $this->upload->display_errors();

				//  echo $name;
				// var_dump($this->upload->data(0));

				// 
			} else {

				// echo  $this->upload->data('file_name');
				date_default_timezone_set('America/Costa_Rica');
				$fecha = date("yy/m/d");
				$name = '../../uploads/' . $this->upload->data('file_name');
				$picture = array(
					"idtreeinfo" => $this->session->idarbol,
					"date" => $fecha,
					"picturefile" => $name
				);
				$insertpicture = $this->user_model->insertpicture($picture);
			}
			// echo $this->session->idarbol;

			// $authVTName = $this->user_model->getViewTreeName($this->session->idarbol);
			// $this->session->set_userdata('pviewtree', $authVTName);
			// $authPicture = $this->user_model->getPicture($this->session->pviewtree->id);
			// $this->session->set_userdata('picture', $authPicture);
			// $this->load->view('include/head.php');
			// $this->load->view('tree/viewtree');
			// $this->load->view('include/footer');

			// $guardoIMG = $this->user_model->insertPhoto();
			redirect(site_url(['user', 'viewtree/' .$this->session->idarbol]));
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user', 'inicio']));
		}
	}

	public function deletepicture($id)
	{
		if ($this->session->has_userdata('user')) {
			$elimino = $this->user_model->deletePhoto($id);
			redirect(site_url(['user', 'viewtree/' . $this->session->idarbol]));
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user', 'inicio']));
		}
	}
	public function viewtree($id)
	{
		if ($this->session->has_userdata('user')) {
			// if($this->session->idarbol != "")
			// {
			// 	$authVTName = $this->user_model->getViewTreeName($this->session->idarbol);
			// 	$this->session->set_userdata('pviewtree', $authVTName);
			// }else{
			$this->session->set_userdata('idarbol', $id);
			$authVTName = $this->user_model->getViewTreeName($id);
			$this->session->set_userdata('pviewtree', $authVTName);
			// }
			// echo $this->session->idarbol;

			$authPicture = $this->user_model->getPicture($this->session->pviewtree->id);
			$this->session->set_userdata('picture', $authPicture);
			// $authTreeFriends = $this->user_model->getTreeFriends($this->session->user->id);
			// $this->session->set_userdata('treefriend', $authTreeFriends);

			$this->load->view('include/head');
			$this->load->view('tree/viewtree');
			$this->load->view('include/footer');
		} else {
			$this->session->set_flashdata('error', 'No ha iniciado sesión');
			redirect(site_url(['user', 'inicio']));
		}
	}



	// public function register()
	// {
	// 	// $this->load->view('user/register');
	// 	if($this->input->post()){
	// 		$col_user = array(
	// 			"name" => $this->input->post('name'),
	// 			"username" => $this->input->post('username'),
	// 			"password" => $this->input->post('password')
	// 		);

	// 		$registerUser = $this->user_model->registerUsuario($col_user);
	// 		// redirect(site_url(['user','dashboard']));
	// 	}else{

	// 		redirect(site_url(['user','inicio']));
	// 	}
	// }

	public function save()
	{
		$validation_errors = [];

		if (!$this->input->post('name')) {
			$validation_errors['name'] = 'is blank';
		}
		if (!$this->input->post('lastname')) {
			$validation_errors['lastname'] = 'is blank';
		}
		if (!$this->input->post('email')) {
			$validation_errors['email'] = 'is blank';
		}
		if (!$this->input->post('phone')) {
			$validation_errors['phone'] = 'is blank';
		}
		if (!$this->input->post('country')) {
			$validation_errors['country'] = 'is blank';
		}
		if (!$this->input->post('address')) {
			$validation_errors['address'] = 'is blank';
		}
		if (!$this->input->post('pass')) {
			$validation_errors['pass'] = 'is blank';
		}
		if (!$this->input->post('repass')) {
			$validation_errors['repass'] = 'is blank';
		}

		if ($this->input->post('repass') != $this->input->post('pass')) {
			$validation_errors['repass'] = 'is blank';
		}

		if (sizeof($validation_errors) > 0) {
			$this->session->set_flashdata('error', 'Faltan ingresar datos');
			redirect(site_url(['user', 'register']));
		} else {
			$user = array(
				"name" => $this->input->post('name'),
				"lastname" => $this->input->post('lastname'),
				"email" => $this->input->post('email'),
				"phone" => $this->input->post('phone'),
				"country" => $this->input->post('country'),
				"address" => $this->input->post('address'),
				"password" => $this->input->post('pass'),
				"rol" => "user"
			);
			$insertUser = $this->user_model->insertarUsuario($user, $this->input->post('email'));
			if ($insertUser) {
				$this->session->set_flashdata('error', 'Usuario Registrado con exito');
			} else {
				$this->session->set_flashdata('error', 'El correo ya se encuentra utilizado');
			}
			redirect(site_url(['user', 'register']));
		}
	}

	public function search($name = '')
	{
		$data['users'] = $this->user_model->getByName($name);
		$this->load->view('user/list', $data);
	}

	public function list()
	{
		$data['users'] = $this->user_model->all();
		$this->load->view('user/list', $data);
	}

	/**
	 * Authenticates a user
	 */
	public function authenticate()
	{
		// get username and password
		$pass = $this->input->post('password');
		$user = $this->input->post('email');

		// check the database with that information
		$authUser = $this->user_model->authenticate($user, $pass);
		// return error or redirect to landing page
		if ($authUser) {
			$this->session->set_userdata('user', $authUser);
			redirect(site_url(['user', 'dashboard']));
		} else {
			$this->session->set_flashdata('error', 'Invalid user name or password');
			redirect(site_url(['user', 'inicio']));
		}
	}
}
