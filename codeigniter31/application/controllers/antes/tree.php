<?php
  require('functions.php');
class Tree{
  /**
 * Guarda el arbol en al base de datos 
 */
    function saveTree($tree) {
        $conn1 = getConnection();
        $conn2 = getConnection();
        $conn3 = getConnection();
        $sql = "INSERT INTO treeinfo (`idtree`, `name`, `heigth`, `date`, `donate`)
                VALUES ('{$tree['idtree']}','{$tree['name']}','{$tree['heigth']}',
                '{$tree['date']}','{$tree['donate']}')";
        $conn1->query($sql);  
        $sqlTF = "SELECT * FROM  treeinfo ORDER BY id DESC LIMIT 1";
        $resultTF = $conn2->query($sqlTF);
        foreach ($resultTF as $TF){
            $sql2 = "INSERT INTO treefriend (`idtreeinfo`,`idfriend`)
            VALUES ('{$TF['id']}','{$tree['idfriend']}')";
        }
  
        $conn3->query($sql2);  
      
        if ($conn3->connect_errno) {
          $conn3->close();
          return false;
        }
        $conn3->close();
        return true;
      }
/**
 * Guarda la imagen en la bd
 */
    function savePicture($picture){
      $conn = getConnection();

      $sql = "INSERT INTO `treepicture`(`idtreeinfo`, `date`, `picturefile`) 
      VALUES ({$picture['idtreeinfo']},'{$picture['date']}',
      '{$picture['picturefile']}')";

      $conn->query($sql);  
      if ($conn->connect_errno) {
        $conn->close();
        return false;
      }
      $conn->close();
      return true;

    }
    
/**
 * Obtiene el tipo de arboles
 */
    function getTrees(){
        $conn = getConnection();
        $sql = "SELECT * FROM tree";
        $result = $conn->query($sql);
      
        if ($conn->connect_errno) {
          $conn->close();
          return [];
        }
        $conn->close();
        return $result;
      }
/**
 * Obtiene solo los arboles de teeinfo
 */
      function getTreesAll(){
        $conn = getConnection();
        $sql = "SELECT * FROM treeinfo";
        $result = $conn->query($sql);
      
        if ($conn->connect_errno) {
          $conn->close();
          return [];
        }
        $conn->close();
        return $result;
      }
/**
 * Obtine todos los arboles de los amigos
 */
      function getTreeFriendsAll(){
        $conn = getConnection();
    
            $sql = "SELECT ti.id, t.specie, ti.name, ti.heigth, ti.date,
        ti.donate FROM treefriend tf INNER JOIN treeinfo ti ON
        ti.id = tf.idtreeinfo INNER JOIN tree t ON t.id = ti.idtree";
        $result = $conn->query($sql);
    
        if ($conn->connect_errno) {
          $conn->close();
          return [];
        }
        $conn->close();
        return $result;
      }

   /**
 * Metodo que actualiza el arbol
 */
  function updatetree($tree){
    $conn = getConnection();
    $sql = "UPDATE treeinfo SET idtree = $tree[idtree], name = '$tree[name]', heigth = $tree[heigth]  WHERE id = $tree[id]";
    // print_r($sql);
    $result = $conn->query($sql);

    if ($conn->connect_errno) {
      $conn->close();
      return [];
    }
    $conn->close();
    return $result;
  }
/**
 * Obtien todo los arboles del los amigos por id
 */ 
function getTreeFriends($idfriend){
    $conn = getConnection();

        $sql = "SELECT ti.id, t.specie, ti.name, ti.heigth, ti.date,
    ti.donate FROM treefriend tf INNER JOIN treeinfo ti ON
    ti.id = tf.idtreeinfo INNER JOIN tree t ON t.id = ti.idtree WHERE tf.idfriend = '$idfriend'";
    $result = $conn->query($sql);

    if ($conn->connect_errno) {
      $conn->close();
      return [];
    }
    $conn->close();
    return $result;
  }

   /**
 * Metodo que obtiene los datos del arbol
 */
  function getTreeInfo($idtreeinfo){
    $conn = getConnection();
        $sql = "SELECT * FROM treeinfo WHERE id = $idtreeinfo";
    $result = $conn->query($sql);

    if ($conn->connect_errno) {
      $conn->close();
      return [];
    }
    $conn->close();
    return $result ->fetch_array();
  }

/**
 * Optine los datos de la base de datos 
 */
  function getImages($idtreeinfo){
    $conn = getConnection();
        $sql = "SELECT * FROM treepicture WHERE idtreeinfo = $idtreeinfo";
    $result = $conn->query($sql);

    if ($conn->connect_errno) {
      $conn->close();
      return [];
    }
    $conn->close();
    return $result;
  }



 /**
 * Guardar arhivo
 *
 * @Metodo que guarda los archivos en el bd
 */
  function uploadPicture($inputName){
    $fileObject = $_FILES[$inputName];

    $target_dir = "images/imgtree/";
    $target_file = $target_dir . basename($fileObject["name"]);

    if (move_uploaded_file($fileObject["tmp_name"], $target_file)) {
          return $target_file;
        } else {
          return false;
        }
  }

 /**

 * @Metodo que elimina los datos de la base de datos
 */
  function deletePicture($idpicture){
    $conn = getConnection();
        $sql = "DELETE FROM treepicture WHERE  id = $idpicture";
    $result = $conn->query($sql);

    if ($conn->connect_errno) {
      $conn->close();
      return true;
    }
    $conn->close();
    return false;
  }

  

}

