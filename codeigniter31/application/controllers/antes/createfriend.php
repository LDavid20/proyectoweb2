<?php
require('friend.php');
$friend = new Friend();
if (
  isset($_POST['name']) && isset($_POST['lastname']) &&
  isset($_POST['email']) && !$friend-> validarEmail($_POST['email']) &&
  isset($_POST['phone']) && isset($_POST['country']) &&
  isset($_POST['address']) && isset($_POST['pass']) &&
  isset($_POST['repass']) && $_POST['pass'] === $_POST['repass']
) {

  $saved =  $friend -> saveFriend($_POST);
  if ($saved) {
    header('Location:  /practica/proyecto/register.php?status=success');
  } else {
    header('Location: /practica/proyecto/register.phpstatus=error');
  }
} else {
  header('Location: /practica/proyecto/register.php?status=error');
}
