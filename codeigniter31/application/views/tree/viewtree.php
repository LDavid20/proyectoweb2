

<div class="container">

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="../dashboard">TREE FRIENDS

    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link" href="../dashboard">Inicio <span class="sr-only">(current)</span></a>
        <?php if ($this->session->user->rol == 'administrador') { ?>
          <a class="nav-item nav-link" href="../adminfriends">Administracion Amigos</a>
          <a class="nav-item nav-link" href="../alltree">Arboles</a>
        <?php } ?>
        <a class="nav-item nav-link active" href="../mytree">Mis Arboles</a>
      </div>

    </div>
    <a class="navbar" href="../logout">Cerrar Seccion</a>
  </nav>
  <?php if ($this->session->user->rol == 'administrador') { ?>
					
				
  <form class="contact__form" method="POST" role="form" action="../updatetree">
    <div class="page-header">
      <h1>Editar Imagen</h1>
    </div>

    <div class="row">
    <div class="col-md-6 form-group">
    <label for="name">Name</label>
        <input class="form-control" type="text" name="name" id="name" value="<?php echo $this->session->pviewtree->name ?>">
      </div>
      <div class="col-md-6 form-group">
        <label for="heigth">Heigth</label>
        <input class="form-control" type="number" min="0" name="heigth" id="heigth" value="<?php echo $this->session->pviewtree->heigth ?>">
      </div>
      <div class="col-md-12 form-group">
      <label class="sr-only" for="">Especie</label>
        <select class="mdb-select form-control" name="idtree" >">
        <option value=""  disabled selected>Seleccione Especie</option>
        <?php
          // $ftree = new Tree();     
          // $trees = $ftree-> getTrees();
          $treesHtml = "";  
          foreach ($this->session->treename as $tree) {
            if($this->session->pviewtree->idtree ==$tree->id){
              $treesHtml .= "<option selected='true' name=\"{$tree->id}\" value={$tree->id}>{$tree->specie}</option>";
            }else{
              $treesHtml .= "<option name=\"{$tree->id}\" value={$tree->id}>{$tree->specie}</option>";
            }
          }
          echo $treesHtml;
        ?>
        </select>
      </div>
      <div class="col-12 mb-3">
        <!-- <img src="images/imgtree/2020.png" alt="" sizes="" srcset=""> -->
        <input name="submit" type="submit" class="btn btn-success" value="Guardar">
        <div class="msg text-center">
          <?php echo $this->session->flashdata('error')?>
        </div>
      </div>
    </div>

<!-- </div> -->
<!-- end form element -->
</form>
  <form enctype="multipart/form-data" class="contact__form" method="POST" role="form" action="../guardarImagen"   >
    <div class="page-header">
      <h1>Agregar Imagen</h1>
    </div>

    <div class="row">
      <div class="col-12">
        <div class="alert alert-success contact__msg" style="display: none" role="alert">
          Tu mensaje fue enviado exitosamente.
        </div>
      </div>
    </div>

    <div class="row">

      <div class="col-md-12 form-group">
        <input type="file" name="picture" id="picture1" require accept="image/*">
      </div>
      <div class="col-12 mb-3">

        <input name="submit" type="submit" class="btn btn-primary" value="Guardar">
        <div class="msg text-center">
          <?php echo $this->session->flashdata('error')?>
        </div>
      </div>
    </div>

</div>
<!-- end form element -->
</form>

<?php } ?>
<div class="container">
  <table class="table table-light ">
    <tbody>
      <thead class="thead-dark">
        <tr>
          <td scope="col">Id</td>
          <td scope="col">Fecha</td>
          <td scope="col">Foto</td>
          <td scope="col">Acción</td>
        </tr>
      </thead>
      <?php
      // $ftree = new Tree();
      // if ($_GET) {
      //   $pictures = $ftree->getImages($_GET['id']);
      // } else {
      //   $pictures = $ftree->getImages($_SESSION['idtreeinfo']);
      // }
      //   $trees = getFriend();
      $treesHtml = "";
      foreach ($this->session->picture as $ptree) {
        if ($this->session->user->rol === "administrador") {
          $treesHtml .= "<tr id='tree_{$ptree->id}'><td>{$ptree->id}</td><td>{$ptree->date}</td><td><img src=\"{$ptree->picturefile}\" width='150px' height='150px'> </td><td> <a href='../deletepicture/{$ptree->id}' class='btn btn-danger' onclick='../deletepicture/{$ptree->id}'>Eliminar</a></td></tr>";
        } else {
          $treesHtml .= "<tr id='tree_{$ptree->id}'><td>{$ptree->id}</td><td>{$ptree->date}</td><td><img src=\"{$ptree->picturefile}\" width='150px' height='150px'> </td></tr>";
        }
      }
      echo $treesHtml;
      ?>
    </tbody>
  </table>
</div>
</div>


