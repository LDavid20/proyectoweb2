


<!-- <link rel="stylesheet" href="css/bootstrap.min.css" >
		<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
		<script src="js/bootstrap.min.js" ></script> -->



<div class="container">

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="dashboard">TREE FRIENDS

		</a>
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link active" href="dashboard">Inicio <span class="sr-only">(current)</span></a>
				<?php
				if ($this->session->user->rol == 'administrador') { ?>
					<a class="nav-item nav-link" href="adminfriends">Administracion Amigos</a>
					<a class="nav-item nav-link" href="alltree">Arboles</a>
				<?php } ?>

				<a class="nav-item nav-link" href="mytree">Mis Arboles</a>
				<!-- <a class="nav-item nav-link disabled" href="#">Mis Arboles</a> -->
			</div>

		</div>
		<a class="navbar" href="logout">Cerrar Seccion</a>
	</nav>
	<div class="jumbotron">
		<h1> Bienvenido <?php echo $this->session->user->name ?> </h1>
		<br />
	</div>
	<blockquote>
 <?php 

		if ($this->session->user->rol == 'administrador') { 
          $count = 0;
          foreach ($this->session->friends as $type) {
              $count += 1;
          }
          $htmlCant = "<p class='text-center'> Cantidad de Amigos $count </p>";
		  echo $htmlCant;
		}
  ?> 

 <?php 
		if ($this->session->user->rol == 'administrador') { 
          $count = 0;
          foreach ($this->session->trees as $typea) {
              $count += 1;
          }
          $htmlCant = "<p class='text-center'> Cantidad de Arboles $count </p>";
		  echo $htmlCant;
		}
  ?> 
		<p>Gracias por ayudar al ambiente</p>
	</blockquote>

</div>


