


<div class="container">

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="dashboard">TREE FRIENDS

		</a>
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link" href="dashboard">Inicio <span class="sr-only">(current)</span></a>
				<?php if ($this->session->user->rol == 'administrador') { ?>
					<a class="nav-item nav-link" href="adminfriends">Administracion Amigos</a>
          <a class="nav-item nav-link active" href="alltree">Arboles</a>
				<?php } ?>
				<a class="nav-item nav-link" href="mytree">Mis Arboles</a>
			</div>

		</div>
		<a class="navbar" href="logout">Cerrar Seccion</a>
	</nav>
  <?php 
          // $fftree = new Tree();
          // $ptrees = $this->load->Tree_model->getTreesAll();
          // $ptrees = $this->Tree_model->getTreesAll();
          $count = 0;
          foreach ($this->session->trees as $typea) {
              $count += 1;
          }
          $htmlCant = "<p class='text-center'> Cantidad de Arboles $count </p>";
          echo $htmlCant;
  ?>
  
  <div class="container">
<table class="table table-light ">
      <tbody>
        <thead class="thead-dark">
        <tr>
          <td  scope="col" >Id</td>
          <td  scope="col" >Especie</td>
          <td  scope="col" >Nombre</td>
          <td  scope="col" >Altura</td>
          <td  scope="col" >Fecha</td>
          <td  scope="col" >Acciones</td>
        </tr>
        </thead>
        <?php
          // $ftree = new Tree();     
          // $trees = $ftree-> getTreeFriendsAll();
        //   $trees = getFriend();
          $treesHtml = "";
          foreach ($this->session->tfull as $tree) {
                  $treesHtml .= "<tr id='tree_{$tree->id}'><td>{$tree->id}</td><td>{$tree->specie}</td><td>{$tree->name}</td><td>{$tree->heigth}</td><td>{$tree->date}</td><td> <a href='viewtree/{$tree->id}' class='btn btn-primary' onclick='viewtree/{$tree->id}'>Ver</a></td></tr>";
          }
          echo $treesHtml;
        ?>
      </tbody>
    </table>
   </div>
</div>

</div>


