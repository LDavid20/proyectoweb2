<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function authenticate($email, $password)
  {
    $query = $this->db->get_where('friend', array('email' => $email, 'password' => $password));
    if ($query->result()) {
      return $query->result()[0];
    } else {
      return false;
    }
  }

  public function admin($email, $password)
  {
    $query = $this->db->get_where('friend', array('email' => $email, 'password' => $password, 'rol' => 'administrador'));
    if ($query->result()) {
      return $query->result()[0];
    } else {
      return false;
    }
  }

  public function getFriends()
  {
    $query = $this->db->get('friend');
    // if ($query->result()) {
    return $query->result();
    // } else {
    //   return false;
    // }
  }
  public function getTrees()
  {
    $query = $this->db->get('treeinfo');
    // if ($query->result()) {
    return $query->result();
    // } else {
    //   return false;
    // }
  }

  public function getTreeName()
  {
    $query = $this->db->get('tree');
    // if ($query->result()) {
    return $query->result();
    // } else {
    //   return false;
    // }
  }
  // $sql = "SELECT ti.id, t.specie, ti.name, ti.heigth, ti.date,
  // ti.donate FROM treefriend tf INNER JOIN treeinfo ti ON
  // ti.id = tf.idtreeinfo INNER JOIN tree t ON t.id = ti.idtree WHERE tf.idfriend = '$idfriend'";
  // $result = $conn->query($sql);
  
  public function getTreeFriends($iduser)
  {
    $query = $this->db->select('ti.id, t.specie, ti.name, ti.heigth, ti.date, ti.donate');
    $query = $this->db->from('treefriend tf');
    $query = $this->db->join('treeinfo ti', 'ti.id = tf.idtreeinfo', 'inner');
    $query = $this->db->join('tree t', 't.id = ti.idtree', 'inner');
    $query = $this->db->where(' tf.idfriend', $iduser);
    $query = $this->db->get();
    return $query->result();
  }

  

  // $query = $this->db->from('tree');
  // $query = $this->db->join('users', 'tree.id_user = users.id_user', 'inner');
  // $query = $this->db->join('especie', 'tree.id_especie = especie.id_especie', 'inner');
  // $query = $this->db->where("tree.id_user", $id_user);
  // $query = $this->db->get();
  function getTreeFriendsAll(){
    $query = $this->db->select('ti.id, t.specie, ti.name, ti.heigth, ti.date, ti.donate');
    $query = $this->db->from('treefriend tf');
    $query = $this->db->join('treeinfo ti', 'ti.id = tf.idtreeinfo', 'inner');
    $query = $this->db->join('tree t', 't.id = ti.idtree');
    $query = $this->db->get();
    return $query->result();

  
    // $conn = getConnection();

  
    //     $sql = "SELECT ti.id, t.specie, ti.name, ti.heigth, ti.date,
    // ti.donate FROM treefriend tf INNER JOIN treeinfo ti ON
    // ti.id = tf.idtreeinfo INNER JOIN tree t ON t.id = ti.idtree";
    // $result = $conn->query($sql);

    // if ($conn->connect_errno) {
    //   $conn->close();
    //   return [];
    // }
    // $conn->close();
    // return $result;
  }

  
  public function getPicture($idtreeinfo)
  {
    $query = $this->db->get_where('treepicture', array('idtreeinfo' => $idtreeinfo)); 
    if ($query->result()) {
      return  $query->result();
    }
    else{
      return [];
    }

  }
  public function getViewTreeName($id)
  {
    $query = $this->db->get_where('treeinfo', array('id' => $id)); 
    if ($query->result()) {
      return  $query->result()[0];
    }
    else{
      return false;
    }

  }
  
  public function insertarTreeInfo($tree)
  {
      $query = $this->db->insert('treeinfo', $tree);
      return $this->db->insert_id();
  }
  public function insertarTreeFriend($tree)
  {
      $query = $this->db->insert('treefriend', $tree);
      return true;
  }
  public function updateTree($idtreeinfo , $datos){
  $query = $this->db->where('id', $idtreeinfo);
  $query =$this->db->update('treeinfo', $datos);
}
public function insertpicture($picture){
  $query = $this->db->insert('treepicture',$picture);
}
public function deletePhoto($id)
{
  $query = $this->db->delete('treepicture', array('id' => $id));
}
 



  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function getByName($name)
  {
    $query = $this->db->get_where('users', array('name' => $name));
    if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }
  }

  /**
   *  Get all users from the databas
   *
   */
  public function all()
  {
    $query = $this->db->get('users');
    return $query->result();
  }

  public function insertarUsuario($friend, $email)
  {
    $query = $this->db->get_where('friend', array('email' => $email));
    if (!$query->result()) {
      $query = $this->db->insert('friend', $friend);
      return  true;
    }

  }
  // public function registerUsuario($col_user){
  //   $query = $this->db->insert('users', $col_user);
  // }

}
